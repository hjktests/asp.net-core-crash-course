﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstWebApp.Models
{
    public class Joke
    {
        // "prop" + 2x Tab creates automatic prop with get & set
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }

        // "ctor" creates constructor function
        public Joke()
        {

        }
    }
}
